package section19;

public class Memento {
    public int balance;

    public Memento(int balance) {
        this.balance = balance;
    }

    public Memento deposit(int amount){
        balance += amount;
        return new Memento(balance);
    }

    public void restore(Memento m){
        balance = m.balance;
    }
}
