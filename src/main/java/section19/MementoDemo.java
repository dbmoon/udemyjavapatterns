package section19;

import static java.lang.System.*;

public class MementoDemo {
    public static void main(String[] args) {
        BankAccount bankAccount = new BankAccount(100);
        Memento m1 = bankAccount.deposit(50);
        Memento m2 = bankAccount.deposit(25);

        out.println(bankAccount);

        //restore to m1
        bankAccount.restore(m1);
        out.println(bankAccount);

        //restore to m2
        bankAccount.restore(m2);
        out.println(bankAccount);
    }
}
