package section22;

public class LightSwitch {

    private State state; // OnState, OffState

    public LightSwitch() {
        this.state = new OffState();
    }

    void on(){
        state.on(this);
    }

    void off(){
        state.off(this);
    }

    public void setState(State state) {
        this.state = state;
    }

}
