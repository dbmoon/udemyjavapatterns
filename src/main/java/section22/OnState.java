package section22;

import static java.lang.System.*;

public class OnState extends State{

    public OnState() {
        out.println("Light turned on");
    }

    @Override
    void off(LightSwitch ls) {
        out.println("Switching light off...");
        ls.setState(new OffState());
    }
}
