package section22;

import static java.lang.System.*;

public class State {

    void on(LightSwitch ls){
        out.println("Light is already on...");
    }

    void off(LightSwitch ls){
        out.println("Light is already off...");
    }
}
