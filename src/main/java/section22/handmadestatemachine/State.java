package section22.handmadestatemachine;

public enum State {
    OFF_HOOK, //starting
    ON_HOOK, // terminal/"final" state
    CONNECTING,
    CONNECTED,
    ON_HOLD
}
