package section22;

public class StateDemo {
    public static void main(String[] args) {
        //Classic Implementation Demo
        LightSwitch lightSwitch = new LightSwitch();
        lightSwitch.on();
        lightSwitch.off();
        lightSwitch.off();
    }
}
