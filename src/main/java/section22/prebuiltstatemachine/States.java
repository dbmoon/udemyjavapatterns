package section22.prebuiltstatemachine;

public enum States {
    OFF_HOOK, //starting
    ON_HOOK, // terminal/"final" state
    CONNECTING,
    CONNECTED,
    ON_HOLD
}
