package section13;

public interface Human {
    void walk();
    void talk();
}
