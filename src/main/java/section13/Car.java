package section13;

public class Car implements Driveable{

    protected Driver driver;

    public Car(Driver driver) {
        this.driver = driver;
    }

    @Override
    public void drive() {
        System.out.println("Car is being driven");
    }
}
