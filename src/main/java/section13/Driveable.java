package section13;

public interface Driveable {
    void drive();
}
