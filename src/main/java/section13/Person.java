package section13;

import static java.lang.System.*;

public class Person implements Human{
    @Override
    public void walk() {
        out.println("I am walking");
    }

    @Override
    public void talk() {
        out.println("I am talking");
    }
}
