package section13;

public class Creature {
    private PropertyProxy<Integer> agility = new PropertyProxy<>(10);

    public void setAgility(int value){
        agility.setValue(value);
    }

    public int getAgility() {
        return agility.getValue();
    }

    @Override
    public String toString() {
        return "Creature{" +
                "agility=" + agility +
                '}';
    }
}
