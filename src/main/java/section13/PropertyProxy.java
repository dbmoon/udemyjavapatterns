package section13;

public class PropertyProxy<T>{
    private T value;

    public PropertyProxy(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        //logging
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PropertyProxy<?> propertyProxy = (PropertyProxy<?>) o;

        return value != null ? value.equals(propertyProxy.value) : propertyProxy.value == null;
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Property{" +
                "value=" + value +
                '}';
    }
}
