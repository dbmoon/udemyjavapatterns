package section13;

import java.lang.reflect.Proxy;

import static java.lang.System.*;

public class ProxyDemo {

    @SuppressWarnings("unchecked")
    public static <T> T withLogging(T target, Class<T> itf)
    {
        return (T) Proxy.newProxyInstance(
                itf.getClassLoader(),
                new Class<?>[] { itf },
                new LoggingHandler(target));
    }

    public static void main(String[] args) {
        Car car = new Car(new Driver(12));
        car.drive();

        CarProxy carProxy = new CarProxy(new Driver(12));
        carProxy.drive();

        //Property Proxy
        Creature creature = new Creature();
        creature.setAgility(89);
        out.println(creature.toString());

        //Dynamic Proxy
        Person person = new Person();
        Human logged = withLogging(person, Human.class);
        logged.talk();
        logged.walk();
        logged.walk();
        out.println(logged);
    }
}
