package section18;

import static java.lang.System.*;

public class FootballCoach {
    public FootballCoach(EventBroker eventBroker){
        eventBroker.subscribe(i -> {
            out.println("Hey, you scored " + i + " goals!");
        });
    }
}
