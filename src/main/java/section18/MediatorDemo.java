package section18;

public class MediatorDemo {
    public static void main(String[] args) {

        ChatRoom chatRoom = new ChatRoom();
        Person john = new Person("John");
        Person jane = new Person("Jane");

        chatRoom.join(john);
        chatRoom.join(jane);

        john.say("hi room");
        jane.say("oh, hey john");

        Person simon = new Person("Simon");
        chatRoom.join(simon);
        simon.say("hi everyone");

        jane.privateMessage("Simon", "Hi there Simon, from Jane");
    }
}
