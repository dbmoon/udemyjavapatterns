package section25.classic.visitor;

abstract class Expression {
    public abstract void accept(ExpressionVisitor visitor);
}
