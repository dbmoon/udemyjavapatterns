package section25.intrusive.visitor;

public class Demo {
    public static void main(String[] args) {
        AdditionExpression e = new AdditionExpression(new DoubleExpression(1),
                new AdditionExpression(new DoubleExpression(2), new DoubleExpression(3)));

        StringBuilder stringBuilder = new StringBuilder();
        e.print(stringBuilder);
        System.out.println(stringBuilder);
    }
}
