package section25.intrusive.visitor;

abstract class Expression {
    public  abstract void print(StringBuilder stringBuilder);
}
