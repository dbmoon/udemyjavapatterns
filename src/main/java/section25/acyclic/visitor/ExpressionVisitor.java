package section25.acyclic.visitor;

public interface ExpressionVisitor extends Visitor{
    void visit(Expression obj);
}
