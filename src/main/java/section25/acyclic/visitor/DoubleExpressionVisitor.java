package section25.acyclic.visitor;

public interface DoubleExpressionVisitor extends Visitor {
    public void visit(DoubleExpression obj);
}
