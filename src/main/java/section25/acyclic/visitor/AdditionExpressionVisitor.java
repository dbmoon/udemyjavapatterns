package section25.acyclic.visitor;

public interface AdditionExpressionVisitor extends Visitor{

    public void visit(AdditionExpression obj);
}
