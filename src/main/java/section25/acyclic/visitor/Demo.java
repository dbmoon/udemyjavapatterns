package section25.acyclic.visitor;

public class Demo {
    public static void main(String[] args) {
        AdditionExpression e = new AdditionExpression(new DoubleExpression(1),
                new AdditionExpression(new DoubleExpression(2), new DoubleExpression(3)));

        ExpressionPrinter expressionPrinter = new ExpressionPrinter();
        expressionPrinter.visit(e);
        System.out.println(expressionPrinter.toString());
    }
}
