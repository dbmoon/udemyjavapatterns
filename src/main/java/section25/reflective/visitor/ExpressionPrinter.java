package section25.reflective.visitor;


public class ExpressionPrinter {
    public static void print(Expression e, StringBuilder stringBuilder){
        if(e.getClass() == DoubleExpression.class){
            stringBuilder.append(((DoubleExpression) e).value);
        }else if(e.getClass() == AdditionExpression.class){
            AdditionExpression ae = (AdditionExpression) e;
            stringBuilder.append("(");
            print(ae.left, stringBuilder);
            stringBuilder.append("+");
            print(ae.right, stringBuilder);
            stringBuilder.append(")");
        }
    }
}
