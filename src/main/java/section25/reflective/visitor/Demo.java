package section25.reflective.visitor;

import static java.lang.System.*;

public class Demo {
    public static void main(String[] args) {
        AdditionExpression e = new AdditionExpression(new DoubleExpression(1),
                new AdditionExpression(new DoubleExpression(2), new DoubleExpression(3)));

        StringBuilder stringBuilder = new StringBuilder();
        ExpressionPrinter.print(e, stringBuilder);
        out.println(stringBuilder);
    }
}
