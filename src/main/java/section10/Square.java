package section10;

public class Square implements Shape{

    private float side;

    public Square(float side) {
        this.side = side;
    }

    public Square() {
    }

    @Override
    public String info() {
        return "A square with a side of " + side;
    }
}
