package section10.adapter.decorator;

import static java.lang.System.out;

public class AdapterDecoratorDemo {
    public static void main(String[] args) {
        MyStringBuilder myStringBuilder = new MyStringBuilder();
        myStringBuilder.append("hello").appendLine(" world");
        out.println(myStringBuilder.concat(" and this too"));
    }
}
