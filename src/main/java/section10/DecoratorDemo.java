package section10;

import static java.lang.System.*;

public class DecoratorDemo {
    public static void main(String[] args) {
        MagicString magicString = new MagicString("Hello");
        out.println(magicString + " has " + magicString.getNumberOfVowels() + " vowels.");

        Circle circle = new Circle(10);
        out.println(circle.info());

        ColoredShape blueSquare = new ColoredShape(new Square(20), "blue");
        out.println(blueSquare.info());

        TransparentShape myCircle =new TransparentShape(new ColoredShape(new Circle(5), "green"), 50);
        out.println(myCircle.info());
    }
}
