package section10.staticdecorators;

import section10.Circle;
import section10.Square;

import static java.lang.System.out;

public class StaticDemo {
    public static void main(String[] args) {
        ColoredShape<Square> blueSquare = new ColoredShape<>(() -> new Square(20), "blue");
        out.println(blueSquare.info());

        TransparentShape<ColoredShape<Circle>> myCircle =
                new TransparentShape<>(() -> new ColoredShape<>(() -> new Circle(5), "green"), 50);
        out.println(myCircle.info());
    }
}
