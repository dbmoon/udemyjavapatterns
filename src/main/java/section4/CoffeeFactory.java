package section4;

import static java.lang.System.out;

public class CoffeeFactory implements HotDrinkFactory{

    @Override
    public HotDrink prepare(int amount) {
        out.println("Grind some beans, boil water, pour " + amount + " ml, add cream, enjoy!");
        return new Coffee();
    }
}
