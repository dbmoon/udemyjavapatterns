package section4;

import static java.lang.System.*;

public class Coffee implements HotDrink{
    @Override
    public void consume() {
        out.println("This coffee is delicious");
    }
}
