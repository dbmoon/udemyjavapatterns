package section4;

public interface HotDrinkFactory {
    HotDrink prepare(int amount);
}
