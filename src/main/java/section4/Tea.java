package section4;

import static java.lang.System.out;

public class Tea implements HotDrink{
    @Override
    public void consume() {
        out.println("This tea is delicious");

    }
}
