package section4;

import static java.lang.System.*;

public class TeaFactory implements HotDrinkFactory{
    @Override
    public HotDrink prepare(int amount) {
        out.println("Put in tea bag, boil water, pour " + amount + " ml, add lemon, enjoy!");
        return new Tea();
    }
}
