package section4;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import static java.lang.System.out;

public class FactoryDemo {
    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, IOException {
        Point point = Point.Factory.newPolarPoint(2,3);
        out.println(point);
        HotDrinkMachine hotDrinkMachine = new HotDrinkMachine();
        HotDrink drink = hotDrinkMachine.makeDrink();
    }
}
