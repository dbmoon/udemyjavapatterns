package section5;

import org.apache.commons.lang3.SerializationUtils;

import static java.lang.System.*;

public class PrototypeDemo {
    public static void main(String[] args) throws CloneNotSupportedException {
        Employee john = new Employee("John", new NewAddress("123 London Road", "London", "UK"));
        Employee chris = new Employee(john);

        chris.name = "Chris";
        out.println(john);
        out.println(chris);

        Foo foo = new Foo(42, "life");
        Foo foo2 = SerializationUtils.roundtrip(foo);

        foo2.whatever = "xyz";
        out.println(foo);
        out.println(foo2);
    }
}
