package section5;

public class Address implements Cloneable{
    public String streetName;
    public int houseNumber;

    public Address(int houseNumber, String streetName) {
        this.houseNumber = houseNumber;
        this.streetName = streetName;
    }

    @Override
    public String toString() {
        return "Address{" +
                "streetName='" + streetName + '\'' +
                ", houseNumber=" + houseNumber +
                '}';
    }

    //deep copy replicate every field
    @Override
    public Object clone() throws CloneNotSupportedException{
        return new Address(houseNumber, streetName);
    }
}
