package section15;

import static java.lang.System.*;

public class BankAccount {

    private int balance;
    private int overdraftLimit = -500;

    public void deposit(int amount){
        balance += amount;
        out.println("Deposited " + amount + ", balance is now " + balance);
    }

    public boolean withdraw(int amount){
        if(balance - amount >= overdraftLimit){
            balance -= amount;
            out.println("Withdrew " + amount + ", balance is now " + balance);
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "balance=" + balance +
                '}';
    }
}
