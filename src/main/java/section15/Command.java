package section15;

public interface Command {
    void call();
    void undo();
}
