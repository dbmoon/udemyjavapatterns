package section15;

public class BankAccountCommand implements Command{

    private BankAccount account;
    private boolean succeeded;

    public enum Action{
        DEPOSIT, WITHDRAW
    }

    private int amount;
    private Action action;

    public BankAccountCommand(BankAccount account, int amount, Action action) {
        this.account = account;
        this.amount = amount;
        this.action = action;
    }

    @Override
    public void call() {
        switch (action){
            case DEPOSIT:
                succeeded = true;
                account.deposit(amount);
                break;
            case WITHDRAW:
                succeeded = account.withdraw(amount);
                break;
        }
    }

    @Override
    public void undo() {
        if(!succeeded) return;
        switch (action){
            case DEPOSIT:
                account.withdraw(amount);
                break;
            case WITHDRAW:
                account.deposit(amount);
                break;
        }
    }
}
