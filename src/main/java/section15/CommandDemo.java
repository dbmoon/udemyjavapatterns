package section15;

import java.util.Collections;
import java.util.List;

import static java.lang.System.*;

public class CommandDemo {
    public static void main(String[] args) {
        BankAccount bankAccount = new BankAccount();
        out.println(bankAccount);

        List<BankAccountCommand> commands = new java.util.ArrayList<>(List.of(
                new BankAccountCommand(bankAccount, 100, BankAccountCommand.Action.DEPOSIT),
                new BankAccountCommand(bankAccount, 1000, BankAccountCommand.Action.WITHDRAW)
        ));

        commands.forEach( c -> {
            c.call();
            out.println(bankAccount);
        });

        Collections.reverse(commands);
        for (Command c: commands){
            c.undo();
            out.println(bankAccount);
        }
    }
}
