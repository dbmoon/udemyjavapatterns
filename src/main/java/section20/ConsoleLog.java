package section20;

import static java.lang.System.*;

public class ConsoleLog implements Log{
    @Override
    public void info(String msg) {
        out.println(msg);
    }

    @Override
    public void warn(String msg) {
        out.println("WARNING: " + msg);
    }
}
