package section20;

public interface Log {
    void info(String msg);
    void warn(String msg);
}
