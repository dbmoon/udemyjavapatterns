package section8;

import static java.lang.System.out;

public class RasterRenderer implements Renderer{
    @Override
    public void renderCircle(float radius) {
        out.println("Drawing pixels for a circle of radius " + radius);
    }
}
