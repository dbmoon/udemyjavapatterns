package section8;

public interface Renderer {
    void renderCircle(float radius);
}
