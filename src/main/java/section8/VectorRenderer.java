package section8;

import static java.lang.System.*;

public class VectorRenderer implements Renderer{
    @Override
    public void renderCircle(float radius) {
        out.println("Drawing a circle of radius " + radius);
    }
}
