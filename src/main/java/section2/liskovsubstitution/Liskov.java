package section2.liskovsubstitution;

import static java.lang.System.out;

public class Liskov {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(2, 3);
        useIT(rectangle);

        Square sq = new Square();
        sq.setWidth(5);
        useIT(sq);
    }

    public boolean isSquare(int width, int height){
        return width == height;
    }

    static void useIT(Rectangle rectangle){
        int width = rectangle.getWidth();
        rectangle.setHeight(10);
        //area = width * 10
        out.println(
                "Expected area of " + (width*10) + ", got " + rectangle.getArea()
        );
    }
}
