package section2.dependencyinversion;

public enum Relationship {
    PARENT, CHILD, SIBLING
}
