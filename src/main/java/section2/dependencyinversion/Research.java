package section2.dependencyinversion;

import org.javatuples.Triplet;

import java.util.List;
import static java.lang.System.out;

public class Research {//High level module, performs our logic, user cares about this more than low level
    public Research(Relationships relationships){
        List<Triplet<Person, Relationship, Person>> relations = relationships.getRelations();
        relations.stream()
                .filter(x -> x.getValue0().name.equals("John") && x.getValue1() == Relationship.PARENT)
                .forEach(objects -> out.println("John has a child called " + objects.getValue2().name));
    }

    public Research(RelationshipBrowser relationshipBrowser){
        List<Person> children = relationshipBrowser.findAllChildrenOf("John");
        children.forEach(x -> out.println("Child Named: " + x.name));
    }
}
