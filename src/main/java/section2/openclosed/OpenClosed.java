package section2.openclosed;

import java.util.List;

import static java.lang.System.out;

public class OpenClosed {
    public static void main(String[] args) {
        Product apple = new Product("Apple", Color.GREEN, Size.SMALL);
        Product tree = new Product("Tree", Color.GREEN, Size.LARGE);
        Product house = new Product("House", Color.BLUE, Size.LARGE);

        List<Product> products = List.of(apple, tree, house);

        ProductFilter pf = new ProductFilter();
        out.println("Green products (old): ");
        pf.filterByColor(products, Color.GREEN).forEach(product -> out.println(" - " + product.name + " is green"));

        BetterFilter bf = new BetterFilter();
        out.println("Green products (new): ");
        bf.filter(products, new ColorSpecification(Color.GREEN)).forEach(product -> out.println(" - " + product.name + " is green"));

        out.println("Large blue items: ");
        bf.filter(products, new AndSpecification<>(
                new ColorSpecification(Color.BLUE),
                new SizeSpecification(Size.LARGE)
        )).forEach(product -> out.println(" - " + product.name + " is a large and blue"));

    }
}
