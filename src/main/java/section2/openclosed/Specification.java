package section2.openclosed;

public interface Specification<T>{
    boolean isSatisfied(T item);
}
