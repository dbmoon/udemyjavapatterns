package section2.singleresponsibility;

import java.io.File;
import java.io.IOException;

import static java.lang.System.out;

public class SingleResponsibility {
    public static void main(String[] args) throws IOException {
        Journal j = new Journal();
        j.addEntry("I watched tv");
        j.addEntry("I am tired");
        out.println(j);
        Persistence persistence = new Persistence();
        String home = System.getProperty("user.home");
        String filename = home + File.separator + "Documents"+ File.separator + "journal.txt";
        persistence.saveToFile(j, filename, true);

        Runtime.getRuntime().exec("notepad.exe " + filename);
    }
}
