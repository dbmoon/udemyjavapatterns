package section2.interfacesegregation;

public interface Scanner {
    void scan(Document d);
}
