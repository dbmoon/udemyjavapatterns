package section2.interfacesegregation;

public interface Printer {
    void print(Document d);
}
