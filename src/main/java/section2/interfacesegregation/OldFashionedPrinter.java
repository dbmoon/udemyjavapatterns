package section2.interfacesegregation;

public class OldFashionedPrinter implements Machine{
    @Override
    public void print(Document document) {
        //
    }

    @Override
    public void fax(Document document) {
        //
    }

    @Override
    public void scan(Document document) {

    }
}
