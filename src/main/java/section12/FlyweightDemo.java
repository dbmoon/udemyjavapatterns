package section12;

import static java.lang.System.out;

public class FlyweightDemo {
    public static void main(String[] args) {
        User2 user = new User2("John Smith");
        User2 user2 = new User2("Jane Smith");

        //Formatted Text Example
        FormattedText formattedText = new FormattedText("This is a brave new world");
        formattedText.capitalize(10, 15);
        out.println(formattedText.toString());

        //Better Formatted Text Example
        BetterFormattedText betterFormattedText = new BetterFormattedText("Make America Great Again");
        betterFormattedText.getRange(13, 18).capitalize = true;
        out.println(betterFormattedText.toString());
    }
}
