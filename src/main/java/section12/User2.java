package section12;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class User2 {
    static List<String> stringList = new ArrayList<>();
    private int[] names;

    public User2(String fullName){
        Function<String, Integer> getOrAdd = (String s) -> {
            int idx = stringList.indexOf(s);
            if (idx != -1) return idx;
            else {
                stringList.add(s);
                return  stringList.size();
            }
        };
        names = Arrays.stream(fullName.split(" ")).mapToInt(getOrAdd::apply).toArray();
    }
}
