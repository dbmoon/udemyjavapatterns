package section21;

import static java.lang.System.*;

public class ObserverDemo implements Observer<Person> {

    public static void main(String[] args) {
        new ObserverDemo();
    }

    public ObserverDemo(){
        Person person = new Person();
        person.subscribe(this);
        for (int i = 20; i < 24; ++i){
            person.setAge(i);
        }
    }

    @Override
    public void handle(PropertyChangedEventArgs<Person> args) {
        out.println("Peron's " + args.propertyName + " has changed to " + args.newValue);
    }
}
