package section21;

public class NewPerson {

    public Event<NewPropertyChangeEventArgs> propertyChanged = new Event<>();

    public int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(this.age == age) return;
        this.age = age;
        propertyChanged.fire(new NewPropertyChangeEventArgs(this, "age"));
    }
}
