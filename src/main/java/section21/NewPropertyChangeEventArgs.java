package section21;

public class NewPropertyChangeEventArgs {
    public Object source;
    public String propertyName;

    public NewPropertyChangeEventArgs(Object source, String propertyName) {
        this.source = source;
        this.propertyName = propertyName;
    }
}
