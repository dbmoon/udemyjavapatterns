package section21;

public interface Observer<T> {
    void handle(PropertyChangedEventArgs<T> args);
}
