package section21;

import static java.lang.System.*;

public class ObserverEventDemo {
    public static void main(String[] args) {
        NewPerson newPerson = new NewPerson();
        Event<NewPropertyChangeEventArgs>.Subscription subscription = newPerson.propertyChanged.addHandler(x -> {
            out.println("Person's " + x.propertyName + " has changed");
        });
        newPerson.setAge(17);
        newPerson.setAge(18);
        subscription.close();
        newPerson.setAge(19);
    }
}
