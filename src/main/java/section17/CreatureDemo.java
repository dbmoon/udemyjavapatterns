package section17;

import static java.lang.System.*;

public class CreatureDemo {
    public static void main(String[] args) {
        Creature creature = new Creature();
        creature.setAgility(12);
        creature.setStrength(17);
        creature.setIntelligence(13);

        out.println(
                "Creature has a max stat of " + creature.max()
                + ", total stats = " + creature.sum()
                + " average stat = " + creature.average()
        );
    }
}
