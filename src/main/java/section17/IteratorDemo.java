package section17;

import static java.lang.System.*;

public class IteratorDemo {
    public static void main(String[] args) {
        //  1
        // / \
        //2   3

        //In order traversal will yield 213
        Node<Integer> root = new Node<Integer>(1, new Node<Integer>(2), new Node<Integer>(3));

         InOrderIterator<Integer> it = new InOrderIterator<>(root);
         while (it.hasNext()){
             out.print("" + it.next() + ",");
         }
         out.println();

         BinaryTree<Integer> tree = new BinaryTree<>(root);
         for (int n: tree){
             out.println("," + n + ",");
         }
    }
}
