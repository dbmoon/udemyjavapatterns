package section17;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.IntStream;

public class Creature implements Iterable<Integer> {

    private final int str = 0;
    private final int agl = 1;
    private final int intl = 2;

    private int [] stats = new int[3];

    public int getStrength(){
        return stats[str];
    }

    public void setStrength(int strength){
        stats[str] = strength;
    }

    public int getAgility(){
        return stats[agl];
    }

    public void setAgility(int agility){
        stats[agl] = agility;
    }

    public int getIntelligence(){
        return stats[intl];
    }

    public void setIntelligence(int intelligence){
        stats[intl] = intelligence;
    }

    public int sum(){
        return IntStream.of(stats).sum();
    }

    public int max(){
        return IntStream.of(stats).max().getAsInt();
    }

    public double average(){
        return IntStream.of(stats).average().getAsDouble();
    }


    @Override
    public Iterator<Integer> iterator() {
        return  IntStream.of(stats).iterator();
    }

    @Override
    public void forEach(Consumer<? super Integer> action) {
        for(int x: stats){
            action.accept(x);
        }
    }

    @Override
    public Spliterator<Integer> spliterator() {
        return IntStream.of(stats).spliterator();
    }
}
