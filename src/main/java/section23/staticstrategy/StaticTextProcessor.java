package section23.staticstrategy;

import section23.HtmlListStrategy;
import section23.ListStrategy;
import section23.MarkdownListStrategy;
import section23.OutputFormat;

import java.util.List;
import java.util.function.Supplier;

public class StaticTextProcessor<LS extends ListStrategy> {
    private StringBuilder stringBuilder = new StringBuilder();
    private LS listStrategy;

    public StaticTextProcessor(Supplier<? extends LS> constructor) {
        this.listStrategy = constructor.get();
    }

    public void setOutputFormat(OutputFormat outputFormat) {
        switch (outputFormat){
            case HTML:
                listStrategy = (LS) new HtmlListStrategy();
                break;
            case MARKDOWN:
                listStrategy = (LS) new MarkdownListStrategy();
                break;
        }
    }

    public void appendList(List<String> items){
        listStrategy.start(stringBuilder);
        for(String item: items){
            listStrategy.addListItem(stringBuilder, item);
        }
        listStrategy.end(stringBuilder);
    }

    public void clear(){
        stringBuilder.setLength(0);
    }

    @Override
    public String toString(){
        return stringBuilder.toString();
    }
}
