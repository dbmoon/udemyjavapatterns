package section23.staticstrategy;

import section23.HtmlListStrategy;
import section23.MarkdownListStrategy;

import java.util.List;

import static java.lang.System.out;

public class StaticStrategyDemo {
    public static void main(String[] args) {
        StaticTextProcessor<MarkdownListStrategy> textProcessor = new StaticTextProcessor<>(MarkdownListStrategy::new);
        textProcessor.appendList(List.of("liberty", "egality", "fraternity"));
        out.println(textProcessor);

        StaticTextProcessor<HtmlListStrategy> textProcessor2 = new StaticTextProcessor<>(HtmlListStrategy::new);
        textProcessor2.appendList(List.of("inheritance", "encapsulation", "polymorphism"));
        out.println(textProcessor2);
    }
}
