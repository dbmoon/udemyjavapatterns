package section23;

import java.util.List;

public class TextProcessor {
    private StringBuilder stringBuilder = new StringBuilder();
    private ListStrategy listStrategy;

    public TextProcessor(OutputFormat outputFormat) {
        setOutputFormat(outputFormat);
    }

    public void setOutputFormat(OutputFormat outputFormat) {
        switch (outputFormat){
            case HTML:
                listStrategy = new HtmlListStrategy();
                break;
            case MARKDOWN:
                listStrategy = new MarkdownListStrategy();
                break;
        }
    }

    public void appendList(List<String> items){
        listStrategy.start(stringBuilder);
        for(String item: items){
            listStrategy.addListItem(stringBuilder, item);
        }
        listStrategy.end(stringBuilder);
    }

    public void clear(){
        stringBuilder.setLength(0);
    }

    @Override
    public String toString(){
        return stringBuilder.toString();
    }
}
