package section23;

public interface ListStrategy {
    default void start(StringBuilder stringBuilder){}
    void addListItem(StringBuilder stringBuilder, String item);
    default void end(StringBuilder stringBuilder){}
}
