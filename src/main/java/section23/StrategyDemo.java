package section23;

import java.util.List;

import static java.lang.System.*;

public class StrategyDemo {
    public static void main(String[] args) {
        TextProcessor textProcessor = new TextProcessor(OutputFormat.MARKDOWN);
        textProcessor.appendList(List.of("liberty", "egality", "fraternity"));
        out.println(textProcessor);

        textProcessor.clear();
        textProcessor.setOutputFormat(OutputFormat.HTML);
        textProcessor.appendList(List.of("inheritance", "encapsulation", "polymorphism"));
        out.println(textProcessor);
    }
}
