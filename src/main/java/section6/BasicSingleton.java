package section6;

import java.io.Serializable;

public class BasicSingleton implements Serializable {

    private static final BasicSingleton INSTANCE = new BasicSingleton();

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    private int value = 0;

    private BasicSingleton(){
    }

    public static  BasicSingleton getInstance(){
        return INSTANCE;
    }

    protected Object readResolve(){
        return INSTANCE;
    }

}
