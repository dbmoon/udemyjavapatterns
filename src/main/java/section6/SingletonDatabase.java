package section6;

import com.google.common.collect.Iterables;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

public class SingletonDatabase {

    private Dictionary<String, Integer> capitals = new Hashtable<>();
    private static int instanceCount = 0;
    public static int getInstanceCount() {return instanceCount;}

    private SingletonDatabase() {
        instanceCount++;
        System.out.println("Initializing a Database");
        try{
            //File f = new File((getClass().getResource("capitals.txt").getFile()));
            Resource resource = new ClassPathResource("capitals.txt");
            InputStream input = resource.getInputStream();
            File f = resource.getFile();
            Path fullPath = Paths.get(f.getPath());
            List<String> lines = Files.readAllLines(fullPath);
            Iterables.partition(lines, 2)
                    .forEach(kv -> capitals.put(kv.get(0).trim(), Integer.parseInt(kv.get(1))));
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final  SingletonDatabase INSTANCE = new SingletonDatabase();

    public static SingletonDatabase getInstance(){
        return INSTANCE;
    }

    public int getPopulation(String name){
        return capitals.get(name);
    }
}
