package section6;

import static java.lang.System.*;

public class LazySingleton {

    private static LazySingleton instance;

    public LazySingleton() {
        out.println("initializing a lazy singleton");
    }

    /*public static synchronized LazySingleton getInstance(){
        if(instance == null){
            instance = new LazySingleton();
        }
        return instance;
    }*/

    //double-checked locking
    public static LazySingleton getInstance(){
        if(instance == null){
            synchronized (LazySingleton.class){
                if(instance == null){
                    instance = new LazySingleton();
                }
            }
        }
        return instance;
    }
}
