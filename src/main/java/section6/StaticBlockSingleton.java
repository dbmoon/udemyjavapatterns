package section6;

import java.io.File;
import java.io.IOException;

import static java.lang.System.*;

public class StaticBlockSingleton {
    private StaticBlockSingleton() throws IOException {
        out.println("Singleton is initializing");
        File.createTempFile(".", ".");
    }

    public static StaticBlockSingleton getInstance() {
        return instance;
    }

    public static void setInstance(StaticBlockSingleton instance) {
        StaticBlockSingleton.instance = instance;
    }

    private static  StaticBlockSingleton instance;

    static {
        try{
            instance = new StaticBlockSingleton();
        }catch (Exception e){
            err.println("failed to create singleton");
        }
    }
}
