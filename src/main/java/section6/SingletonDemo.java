package section6;

import java.io.*;

import static java.lang.System.out;

public class SingletonDemo {
    public static void main(String[] args) throws Exception {
        /*BasicSingleton singleton = BasicSingleton.getInstance();
        singleton.setValue(123);
        out.println(singleton.getValue())*/;

        //Basic singleton can be bypassed by reflection amd serialization
        BasicSingleton basicSingleton = BasicSingleton.getInstance();
        basicSingleton.setValue(111);

        String home = System.getProperty("user.home");
        String filename = home + File.separator + "Documents"+ File.separator + "singleton.bin";

        saveToFile(basicSingleton, filename);
        basicSingleton.setValue(222);

        BasicSingleton basicSingleton1 = readFromFile(filename);

        out.println(basicSingleton == basicSingleton1);

        out.println(basicSingleton.getValue());
        out.println(basicSingleton1.getValue());

        out.println("-----------ENUM BASED EXAMPLES-----------");

        String filename2 = home + File.separator + "Documents"+ File.separator + "myfile.bin";
        EnumBasedSingleton enumBasedSingleton = EnumBasedSingleton.INSTANCE;
        //enumBasedSingleton.setValue(111);
        saveToFileEnumBased(enumBasedSingleton, filename2);
        EnumBasedSingleton enumBasedSingleton1 = readFromFileEnumBased(filename2);
        out.println(enumBasedSingleton1.getValue());


        out.println("-----------MONOSTATE EXAMPLES-----------");
        ChiefExecutiveOfficer chiefExecutiveOfficer = new ChiefExecutiveOfficer();
        chiefExecutiveOfficer.setAge(55);
        chiefExecutiveOfficer.setName("Adam Smith");

        ChiefExecutiveOfficer chiefExecutiveOfficer1 = new ChiefExecutiveOfficer();
        out.println(chiefExecutiveOfficer1);
    }

    static void saveToFile(BasicSingleton singleton, String filename) throws Exception{

        try (FileOutputStream fileOutputStream = new FileOutputStream(filename);
             ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream)){
            outputStream.writeObject(singleton);
        }
    }

    static BasicSingleton readFromFile(String filename) throws Exception{
        try(FileInputStream fileInputStream = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(fileInputStream)){
            return (BasicSingleton)  in.readObject();
        }
    }
    static void saveToFileEnumBased(EnumBasedSingleton singleton, String filename) throws Exception{

        try (FileOutputStream fileOutputStream = new FileOutputStream(filename);
             ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream)){
            outputStream.writeObject(singleton);
        }
    }

    static EnumBasedSingleton readFromFileEnumBased(String filename) throws Exception{
        try(FileInputStream fileInputStream = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(fileInputStream)){
            return (EnumBasedSingleton)  in.readObject();
        }
    }

}
