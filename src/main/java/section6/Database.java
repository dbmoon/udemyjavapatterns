package section6;

public interface Database {
    int getPopulation(String name);
}
