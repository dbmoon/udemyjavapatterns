package section6;

public enum Subsystem {
    PRIMARY,
    AUXILIARY,
    FALLBACK
}
