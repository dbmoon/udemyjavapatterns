package section14;

public class BrokerDoubleAttackModifier extends BrokerCreatureModifier implements AutoCloseable{

    private final int token;

    public BrokerDoubleAttackModifier(Game game, BrokerCreature creature) {
        super(game, creature);

        token = game.queries.subscribe(q -> {
           if(q.creatureName.equals(creature.name) && q.argument == Query.Argument.ATTACK){
               q.result *= 2;
           }
        });
    }

    @Override
    public void close(){
        game.queries.unsubscribe(token);
    }
}
