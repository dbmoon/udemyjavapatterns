package section14;

import static java.lang.System.out;

public class IncreaseDefenseModifier extends CreatureModifier{
    public IncreaseDefenseModifier(Creature creature) {
        super(creature);
    }

    @Override
    public void handle() {
        out.println("Increasing " + creature.name + "'s defense");
        creature.defense += 3;
        super.handle();
    }
}
