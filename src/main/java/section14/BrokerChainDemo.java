package section14;

import static java.lang.System.out;

public class BrokerChainDemo {
    public static void main(String[] args) {
        Game game = new Game();
        BrokerCreature goblin = new BrokerCreature(game, "Strong Goblin", 2, 2);
        out.println(goblin);

        BrokerIncreaseDefenceModifier icm = new BrokerIncreaseDefenceModifier(game, goblin);
        BrokerDoubleAttackModifier dam = new BrokerDoubleAttackModifier(game, goblin);

        try(dam){
            out.println(goblin);
        }

        out.println(goblin);
    }
}
