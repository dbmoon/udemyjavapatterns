package section14;

public class Query {
    public String creatureName;

    public Query(String creatureName, Argument argument, int result) {
        this.creatureName = creatureName;
        this.argument = argument;
        this.result = result;
    }

    enum Argument{
        ATTACK, DEFENSE
    }
    public Argument argument;
    public int result;
}
