package section14;

import static java.lang.System.out;

public class NoBonusModifier extends CreatureModifier{
    public NoBonusModifier(Creature creature) {
        super(creature);
    }

    @Override
    public void handle() {
        //
        out.println("No bonuses for you");
    }
}
