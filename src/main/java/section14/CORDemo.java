package section14;

import static java.lang.System.*;

public class CORDemo {
    public static void main(String[] args) {
        Creature creature = new Creature("Goblin", 2, 2);
        out.println(creature);

        CreatureModifier root = new CreatureModifier(creature);

        //To break chain add modifier that doesnt call super.handle
        //root.add(new NoBonusModifier(creature));

        out.println("Let's double goblin's attack...");
        root.add(new DoubleAttackModifier(creature));

        out.println("Let's increase goblin's defense...");
        root.add(new IncreaseDefenseModifier(creature));

        root.handle();
        out.println(creature);
    }
}
