package section14;

public class BrokerCreature {
    private Game game;

    public BrokerCreature(Game game, String name, int baseAttack, int baseDefence) {
        this.game = game;
        this.name = name;
        this.baseAttack = baseAttack;
        this.baseDefence = baseDefence;
    }

    public String name;
    public int baseAttack, baseDefence;

    public Game getGame() {
        return game;
    }

    public String getName() {
        return name;
    }

    public int getAttack() {
        Query q = new Query(name, Query.Argument.ATTACK, baseAttack);
        game.queries.fire(q);
        return q.result;
    }

    public int getDefence()  {
        Query q = new Query(name, Query.Argument.DEFENSE, baseDefence);
        game.queries.fire(q);
        return q.result;
    }

    @Override
    public String toString() {
        return "BrokerCreature{" +
                "game=" + game +
                ", name='" + name + '\'' +
                ", baseAttack=" + getAttack() +
                ", baseDefence=" + getDefence() +
                '}';
    }
}
