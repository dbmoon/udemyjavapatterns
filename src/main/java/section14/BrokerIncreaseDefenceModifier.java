package section14;

public class BrokerIncreaseDefenceModifier extends BrokerCreatureModifier {

    public BrokerIncreaseDefenceModifier(Game game, BrokerCreature creature) {
        super(game, creature);

       game.queries.subscribe(q -> {
            if(q.creatureName.equals(creature.name) && q.argument == Query.Argument.DEFENSE){
                q.result += 3;
            }
        });
    }
}
