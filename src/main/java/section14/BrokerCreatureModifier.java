package section14;

public class BrokerCreatureModifier {

    protected Game game;
    protected BrokerCreature creature;

    public BrokerCreatureModifier(Game game, BrokerCreature creature) {
        this.game = game;
        this.creature = creature;
    }
}
