package section3;

public class NewPersonJobBuilder extends NewPersonBuilder{
    public NewPersonJobBuilder(NewPerson newPerson){
        this.newPerson = newPerson;
    }

    public NewPersonJobBuilder worksAt(String companyName){
        newPerson.companyName = companyName;
        return this;
    }

    public NewPersonJobBuilder asA(String position){
        newPerson.position = position;
        return this;
    }

    public NewPersonJobBuilder withIncome(int annualIncome){
        newPerson.annualIncome = annualIncome;
        return this;
    }
}
