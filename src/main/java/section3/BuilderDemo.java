package section3;

import static java.lang.System.*;

public class BuilderDemo {
    public static void main(String[] args) {
        String hello = "hello";
        //out.println("<p>" + hello + "</p>");

        String [] words = {"hello", "world"};

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<ul>\n");
        for(String word: words){
            stringBuilder.append(String.format("  <li>%s</li>\n", word));
        }
        stringBuilder.append("</ul>");
        /*out.println(
                stringBuilder
        );*/

        HtmlBuilder builder = new HtmlBuilder("ul");
        builder.addChild("li", "hello")
                .addChild("li", "world");
        out.println(builder);

        StringBuilder sb = new StringBuilder();
        sb.append("foo").append("bar");

        EmployeeBuilder employeeBuilder = new EmployeeBuilder();
        Person dmitri = employeeBuilder.withName("Dmitri").worksAt("Developer").build();
        out.println(dmitri);

        out.println("MULTI FACETED BUILDER");
        NewPersonBuilder newPersonBuilder = new NewPersonBuilder();
        NewPerson newPerson = newPersonBuilder
                .lives()
                .at("Rooitou Street")
                .inCity("Roodepoort")
                .withPostcode("1709")
                .works()
                .asA("Developer")
                .worksAt("FNB")
                .withIncome(100)
                .build();
        out.println(newPerson);
    }
}
