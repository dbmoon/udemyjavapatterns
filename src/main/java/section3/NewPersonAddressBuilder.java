package section3;

public class NewPersonAddressBuilder extends NewPersonBuilder{
    public NewPersonAddressBuilder(NewPerson newPerson){
        this.newPerson = newPerson;
    }

    public NewPersonAddressBuilder at(String streetAddress){
        newPerson.streetAddress = streetAddress;
        return this;
    }

    public NewPersonAddressBuilder withPostcode(String postcode){
        newPerson.postcode = postcode;
        return this;
    }

    public NewPersonAddressBuilder inCity(String city){
        newPerson.city = city;
        return this;
    }
}
