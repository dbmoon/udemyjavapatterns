package section3;

public class NewPersonBuilder {
    protected  NewPerson newPerson = new NewPerson();

    public NewPersonAddressBuilder lives(){
        return new NewPersonAddressBuilder(newPerson);
    }

    public NewPersonJobBuilder works(){
        return new NewPersonJobBuilder(newPerson);
    }

    public NewPerson build(){
        return newPerson;
    }
}
