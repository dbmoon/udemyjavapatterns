package section6;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class SingletonRecordFinderTest {

    @Test
    public void singletonTotalPopulationTest() {
        SingletonRecordFinder singletonRecordFinder = new SingletonRecordFinder();
        List<String> names = List.of("Jakarta", "Delhi");
        int tp = singletonRecordFinder.getTotalPopulation(names);
        assertEquals(143000+23000, tp);
    }

    @Test
    public void dependentPopulationTest() {
        DummyDatabase dummyDatabase = new DummyDatabase();
        ConfigurableRecordFinder configurableRecordFinder = new ConfigurableRecordFinder(dummyDatabase);
        List<String> names = List.of("alpha", "gamma");
        int tp = configurableRecordFinder.getTotalPopulation(names);
        assertEquals(1+3, tp);
    }

}